# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://klarala@bitbucket.org/klarala/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/klarala/stroboskop/commits/5fe136281ca6136fbec848a00491f38626c58e50

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/klarala/stroboskop/commits/dc445254e0620f316c8694e8935812d1b7b893a6

Naloga 6.3.2:
https://bitbucket.org/klarala/stroboskop/commits/b6f25a9615da8acee929947431bc6da7522640e6

Naloga 6.3.3:
https://bitbucket.org/klarala/stroboskop/commits/374c4a1e37741af4ac7c1ef29ab6769f9a8b2035

Naloga 6.3.4:
https://bitbucket.org/klarala/stroboskop/commits/6fa402e05bf3fc3a23070f8df109786bbc8c479b

Naloga 6.3.5:

```
git checkout master
git merge izgled
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/klarala/stroboskop/commits/faf7bed2d679a8e06bd5a70e055c48b7d2130779

Naloga 6.4.2:
https://bitbucket.org/klarala/stroboskop/commits/386745ff54a7e890191de1bab9f7ae81c2794467

Naloga 6.4.3:
https://bitbucket.org/klarala/stroboskop/commits/7453a6c7f95d4273ce987edd08b643f77d7b70a6

Naloga 6.4.4:
https://bitbucket.org/klarala/stroboskop/commits/569d82f62b186caf3f95cda985fe9fc4b55c66ab